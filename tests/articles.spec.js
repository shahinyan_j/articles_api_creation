const request = require("supertest");
const { app } = require("../server");
const jwt = require("jsonwebtoken");
const config = require("../config");
const mongoose = require("mongoose");
const mockingoose = require("mockingoose");
const Article = require("../api/articles/articles.schema");
const User = require("../api/users/users.model");

describe("API articles testing", () => {
  let token;
  const USER_ID = new mongoose.Types.ObjectId();
  const ARTICLE_ID = "fake";
  const MOCK_DATA_USER = {
    _id: USER_ID,
    name: "jules",
    email: "test@test.com",
    role: "admin",
  };
  const MOCK_ARTICLE_DATA = {
    title: "test title",
    content: "test content",
  };
  const MOCK_ARTICLE_DATA_CREATED = {
    _id: ARTICLE_ID,
    title: "created title",
    content: "created content",
    user: USER_ID,
  };

  beforeEach(() => {
    token = jwt.sign({ user: USER_ID }, config.secretJwtToken);
    mockingoose(User).toReturn(MOCK_DATA_USER, "findOne");
    mockingoose(Article)
      .toReturn(MOCK_ARTICLE_DATA_CREATED, "save")
      .toReturn(MOCK_ARTICLE_DATA_CREATED, "findOneAndUpdate");
  });

  test("[Articles] Create Article", async () => {
    const res = await request(app)
      .post("/api/articles")
      .set("x-access-token", token)
      .send({ ...MOCK_ARTICLE_DATA, user: USER_ID });
    console.log(res.body);
    expect(res.status).toBe(201);
    expect(res.body.user.toString()).toBe(
      MOCK_ARTICLE_DATA_CREATED.user.toString()
    );
    expect(res.body.title).toBe(MOCK_ARTICLE_DATA_CREATED.title);
    expect(res.body.content).toBe(MOCK_ARTICLE_DATA_CREATED.content);
  });

  test("[Articles] Update Article", async () => {
    const res = await request(app)
      .put(`/api/articles/${ARTICLE_ID}`)
      .set("x-access-token", token)
      .send(ARTICLE_ID, { ...MOCK_ARTICLE_DATA, user: USER_ID });
    expect(res.status).toBe(201);
    expect(res.body).toHaveProperty("_id");
    expect(res.body.user.toString()).toBe(MOCK_DATA_USER._id.toString());
    expect(res.body.title).toBe(MOCK_ARTICLE_DATA_CREATED.title);
    expect(res.body.content).toBe(MOCK_ARTICLE_DATA_CREATED.content);
  });

  test("[Articles] Delete Article", async () => {
    const res = await request(app)
      .delete(`/api/articles/${ARTICLE_ID}`)
      .set("x-access-token", token)
      .send();
    expect(res.status).toBe(204);
  });

  afterEach(() => {
    mockingoose.resetAll();
    jest.restoreAllMocks();
  });
});
