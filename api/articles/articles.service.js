const User = require("../users/users.model");
const Article = require("./articles.schema");

class ArticlesService {
  getAll() {
    return Article.find({});
  }
  create(data) {
    const article = new Article(data);
    return article.save();
  }
  update(id, data) {
    return Article.findByIdAndUpdate(id, data, { new: true });
  }
  delete(id) {
    return Article.deleteOne({ _id: id });
  }
  async isAdmin(id) {
    const user = await User.findById(id);
    if (user.role !== "admin") {
      return false;
    }
    return true;
  }
  async getArticlesByUserId(userId) {
    const articles = await Article.find({ user: userId }).populate(
      "user",
      "-password"
    );
    return articles;
  }
}

module.exports = new ArticlesService();
